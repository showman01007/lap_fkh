﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{

    public class AdminProductVM
    {
        public List<UnitVM> UnitList { get; set; }
        public int UnitListSelection { get; set; }
        public List<CategoryVM> CatList { get; set; }
        public int CategoryListSelection { get; set; }
        public List<ManufacturerVM> ManuList { get; set; }
        public int ManufacturerListSelection { get; set; }
        public int CampWas { get; set; }
        public string ProductName { get; set; }
        public string ProductDescr { get; set; }
        public decimal UnitPrice { get; set; }
        public string ImagePath { get; set; }
        public decimal UnitAmount { get; set; }

        public AdminProductVM()
        {
            UnitList = new List<UnitVM>();
            CatList = new List<CategoryVM>();         
            ManuList = new List<ManufacturerVM>();
        }
    }
}