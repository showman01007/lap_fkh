﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Feinkosthandel.Models
{
    public class RegisterVM
    {
        public List<string> TitleList { get; }
        [Required(ErrorMessage = "PFlichtfeld")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "PFlichtfeld")]
        [MinLength(8)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Pflichtfeld")]
        public string Title { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "PFlichtfeld")]
        [Compare("Password")]
        public string PasswordConfirm { get; set; }
        [Required(ErrorMessage = "PFlichtfeld")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "PFlichtfeld")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "PFlichtfeld")]
        public string Street { get; set; }
        [Required(ErrorMessage = "PFlichtfeld")]
        public int ZipCode { get; set; }
        [Required(ErrorMessage = "PFlichtfeld")]
        public string City { get; set; }
        [Required(ErrorMessage = "PFlichtfeld")]
        public bool HasReadTerms { get; set; }

        
    }
}