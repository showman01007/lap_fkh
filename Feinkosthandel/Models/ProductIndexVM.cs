﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{
    public class ProductIndexVM
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }
        public string Category { get; set; }
        public string ProductName { get; set;}
        public string Manufacturer { get; set; }
        public decimal UnitPriceGross { get; set; }
        public string Unit { get; set; }
        public string UnitAbbreviation { get; set; }
        public decimal UnitAmount { get; set; }
        [Range(1,100)]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Count must be a natural number")]
        public int CampWas { get; set; }

        public string ImagePath { get; set; }
    }
}