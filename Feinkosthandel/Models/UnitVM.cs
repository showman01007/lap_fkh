﻿namespace Feinkosthandel.Models
{
    public class UnitVM
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public string UnitAbbreviation { get; set; }
    }
}