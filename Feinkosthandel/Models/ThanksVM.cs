﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{
    public class ThanksVM
    {
        public int OrderId { get; set; }
        public DateTime Date { get; set; }
        public string Street { get; set; }
        public int Zip { get; set; }
        public string City { get; set; }
        public string PaymentName { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public decimal TotalPriceGross { get; set; }
        
    }
}