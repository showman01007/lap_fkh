﻿namespace Feinkosthandel.Models
{
    public class ManufacturerVM
    {
        public int ManuId { get; set; }
        public string ManufacturerName { get; set; }
    }
}