﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{
    public class CategorySearchVM
    {
        public List<CategoryVM> Catlist;
        public int CatSelected;
        public string categoryName { get; set; }

        public CategorySearchVM()
        {
            Catlist = new List<CategoryVM>();
        }
        
    }
}