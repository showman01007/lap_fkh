﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{
    public class CategoryVM
    {
        public int id { get; set; }
        public string name { get; set; }        
        public int productcounter { get; set; }
    }
}