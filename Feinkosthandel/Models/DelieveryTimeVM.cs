﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{
    public class DelieveryTimeVM
    {
        public int DelieverTimeId { get; set; }
        public string DelieverTimeName { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}