﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Feinkosthandel.Models
{
    public class ShoppingCartIndexVM
    {
        public int ProductId { get; set; }
        public int ShoppingCartId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public decimal UnitAmount { get; set; }
        public string UnitAbbreviation { get; set; }
        public string Manufacturer { get; set; }
        public int Amount { get; set; }
        public decimal LinePrice { get; set; }
    }
}