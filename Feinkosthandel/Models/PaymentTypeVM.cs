﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{
    public class PaymentTypeVM
    {
        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
    }
}