﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{
    public class ProductDetailVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Manufacturer { get; set; }
        public decimal UnitPriceGross { get; set; }
        public string Unit { get; set; }
        public string UnitAbbreviation { get; set; }
        public decimal UnitAmount { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal CategoryTaxRate { get; set; }
        public decimal PricePerOneUnit { get; set; }
        public string ImagePath { get; set; }
        public string UnitName { get; set; }
    }
}