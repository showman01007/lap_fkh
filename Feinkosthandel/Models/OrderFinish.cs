﻿using Feinkosthandel.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Models
{
    public class OrderFinish
    {
        public int OrderId { get; set; }

        public decimal ProductsTotal { get; set; }
        public decimal TotalPrice { get; set; }
        [Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        
        //[MyDate(ErrorMessage = "Back date entry not allowed")]
        public DateTime DelieveryDate { get; set; }
        public List<ShoppingCartIndexVM> ShoppingCart { get; set; }
        public string Titel { get; set; }
        [Display(Name ="FirstName:")]
        public string Vorname { get; set; }
        [Display(Name = "LastName:")]
        public string Nachname { get; set; }
        [Display(Name = "Adress:")]
        public string RechnungsStreet { get; set; }
        [Display(Name = "PLZ:")]
        public int RechnungsZip { get; set; }
        [Display(Name = "City:")]
        public string RechnungsCity { get; set; }
        [Display(Name = "Adress:")]
        public string Lieferadresse { get; set; }
        [Display(Name = "PLZ:")]
        public int LieferZip { get; set; }
        [Display(Name = "City:")]
        public string LieferCity { get; set; }
        public List<PaymentTypeVM> PaymentTypeList { get; set; }
        public List<DelieveryTimeVM> DelieveryTimeList { get; set; }

        //Selection from Dropdown in diesen Variablen speichern
        public int Payment { get; set; }
        public int Delievery { get; set; }

        public OrderFinish()
        {
            ShoppingCart = new List<ShoppingCartIndexVM>();
            PaymentTypeList = new List<PaymentTypeVM>();
            DelieveryTimeList = new List<DelieveryTimeVM>();
        }
    }
}