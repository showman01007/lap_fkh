﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Feinkosthandel.Models;
using Feinkosthandel.Logic.DAL;

namespace Feinkosthandel.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _IndexLeft()
        {
            using (var db = new FeinkosthandelEntities())
            {
                var dbCategory = db.Category.ToList();
                List<CategoryVM> vmCatList = new List<CategoryVM>();

                foreach (var item in dbCategory)
                {
                    vmCatList.Add(new CategoryVM
                    {
                        id = item.Id,
                        name = item.Name
                    });
                }
                ViewBag.counter = vmCatList.Count();
                return PartialView(vmCatList);
            }
        }

        public ActionResult _IndexMain()
        {
            using (var db = new FeinkosthandelEntities())
            {
                var vmProducts = db.ProductInfo.ToList();
                List<ProductIndexVM> prodList = new List<ProductIndexVM>();
                foreach (var item in vmProducts)
                {
                    prodList.Add(new ProductIndexVM
                    {
                        Id = item.ProductId,
                        ProductName = item.Name,
                        Category = item.CategoryName,
                        UnitAmount = item.UnitAmount,
                        Manufacturer = item.ManufacturerName,
                        UnitPriceGross = item.UnitPriceGross ?? -1m,
                        Unit = item.UnitName,
                        UnitAbbreviation = item.UnitAbbreviation,
                        ImagePath = item.ImagePath,

                    });
                }
                return PartialView(prodList);
            }
        }
    }
}