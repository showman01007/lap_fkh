﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Feinkosthandel.Logic;

namespace Feinkosthandel.Controllers
{
    public class GameController : Controller
    {
        // GET: Game
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult Index2(int id)
        {
            if (id==1)
            {
                ViewBag.Win = GameManager.didWin();
                return RedirectToAction("Winner");
            }
            return RedirectToAction("NoWinner");
        }

        public ActionResult Winner()
        {
            return View();
        }

        public ActionResult NoWinner()
        {
            return View();
        }
    }
}