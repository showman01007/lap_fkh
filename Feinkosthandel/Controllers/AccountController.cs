﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using Feinkosthandel.Models;

namespace Feinkosthandel.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginVM vmLogin)
        {
            var canLogin = Logic.CustomerManager.IsLoginValid(vmLogin.Email, vmLogin.Password);

            if (canLogin)
            {
                var dbAccount = Logic.CustomerManager.LoadAccount(vmLogin.Email);

                var success = LogUserIn(dbAccount.CustomerId, dbAccount.Email);

                if (success)
                {
                    return RedirectToAction("Index", "Product");
                }
                //login
            }


            
            // 0. Prüfen ob Email auch eine Email ist
            // 1. Prüfen ob Benutzer existiert
            // 2. Wenn existiert, Salt und PwHash laden 
            // 3. dbSalt an vmLogin.Password anhängen
            // 4. Den Wert Hashen 
            // 5. Den neuen Hash mit dem aus der DB vergleichen
            // 6. Wenn erfolgreich, Benutzer wirklich einloggen
            //  und weiterleiten
            // 7. Ansonsten: Fehlermeldung ausgeben und zurück auf 
            //  die Seite
            return View();
        }

        private bool LogUserIn(int customerId, string email)
        {
            // isPersistent hier auf false gesetzt ist quasi die Remember Me Funktion, könnte mit CheckBox eingebaut werden und anhand dieser gesetzt werden
            //true ... auch nach Schließung des Browsers bleibt cookie gespeichert
            var authTicket = new FormsAuthenticationTicket(0,customerId.ToString(),DateTime.Now,DateTime.Now.AddHours(1),false,"");

            //AuthTicket verschlüsseln
            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

            //Cookie erzeugen
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

            //Coookie dem Benutzer schicken
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);

            Session["UserEmail"] = email;

            return true;
        }

        [HttpGet]
        public ActionResult Register ()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterVM regVM)
        {
            if (!ModelState.IsValid)
            {
                return View(regVM);
            }

            if (regVM.Title == "null" || string.IsNullOrEmpty(regVM.Title))
            {
                return View(regVM);
            }

            if (!regVM.HasReadTerms)
            {
                ViewBag.termerror = "Read terms and accept them";
                return View(regVM);
            }

            var success = Logic.CustomerManager.addNewCustomer(
                regVM.City, regVM.Email, regVM.FirstName, regVM.LastName, regVM.Street, regVM.Title, regVM.ZipCode, regVM.Password);

            return RedirectToAction("Index", "Product");
                
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}