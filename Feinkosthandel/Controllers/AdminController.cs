﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Feinkosthandel.Models;
using Feinkosthandel.Logic;

namespace Feinkosthandel.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CreateProduct()
        {
            AdminProductVM vmProduct = new AdminProductVM();
            vmProduct.CatList = ProductManager.getAllCategories();
            vmProduct.ManuList = AdminManager.getAllManufacturer();
            vmProduct.UnitList = AdminManager.getAllUnits();
            return View(vmProduct);
        }

        [HttpPost]
        public ActionResult CreateProduct(AdminProductVM vmProduct)
        {
            vmProduct.ImagePath = "~" + vmProduct.ImagePath;
            AdminManager.addProducts(vmProduct);
            return RedirectToAction("CreateProduct");
        }

        [HttpGet]
        public ActionResult EditProduct()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EditProduct(AdminProductVM vmProduct)
        {
            AdminManager.updateProducts(vmProduct);
            return RedirectToAction("CreateProduct");
        }

        [HttpPost]
        public ActionResult CreateUnit(string unitname, string unitabbreviation)
        {
            bool success = AdminManager.createUnit(unitname, unitabbreviation);
            if(success)
            {
                TempData["ConfirmMessage"] = $"{unitname} erfolgreich angelegt";
            }
            else
            {
                TempData["ErrorMessage"]=$"{unitname} konnte nicht angelegt werden";
            }
            return RedirectToAction("CreateProduct");
        }

        [HttpPost]
        public ActionResult DeleteUnit(int uid)
        {
            bool success = AdminManager.deleteUnit(uid);
            if (success)
            {
                TempData["ConfirmMessage"] = "Erfolgreich entfernt";
            }
            else
            {
                TempData["ErrorMessage"] = "Konnte nicht entfernt werden";
            }
            return RedirectToAction("CreateProduct");
        }

        [HttpPost]
        public ActionResult CreateCategory(string categoryname)
        {
            bool success = AdminManager.createCategory(categoryname);
            if (success)
            {
                TempData["ConfirmMessage"] = $"{categoryname} erfolgreich angelegt";
            }
            else
            {
                TempData["ErrorMessage"] = $"{categoryname} konnte nicht angelegt werden";
            }
            return RedirectToAction("CreateProduct");
        }

        [HttpPost]
        public ActionResult DeleteCategory(int cid)
        {
            bool success = AdminManager.deleteCategory(cid);
            if (success)
            {
                TempData["ConfirmMessage"] = "Erfolgreich entfernt";
            }
            else
            {
                TempData["ErrorMessage"] = "Konnte nicht entfernt werden";
            }
            return RedirectToAction("CreateProduct");
        }

        [HttpPost]
        public ActionResult CreateManufacturer(string manufacturername)
        {
            bool success = AdminManager.createManufacturer(manufacturername);
            if (success)
            {
                TempData["ConfirmMessage"] = $"{manufacturername} erfolgreich angelegt";
            }
            else
            {
                TempData["ErrorMessage"] = $"{manufacturername} konnte nicht angelegt werden";
            }
            return RedirectToAction("CreateProduct");
        }

        [HttpPost]
        public ActionResult DeleteManufacturer(int mid)
        {
            bool success = AdminManager.deleteManufacturer(mid);
            if (success)
            {
                TempData["ConfirmMessage"] = "Erfolgreich entfernt";
            }
            else
            {
                TempData["ErrorMessage"] = "Konnte nicht entfernt werden";
            }
            return RedirectToAction("CreateProduct");
        }
    }
}