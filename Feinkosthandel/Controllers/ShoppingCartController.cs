﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Feinkosthandel.Logic;
using Feinkosthandel.Models;
using Feinkosthandel.Logic.DAL;

namespace Feinkosthandel.Controllers
{
    public class ShoppingCartController : Controller
    {
        [Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            //Waren korb von user aus db laden
            var dbCart = ShoppingCartManager.LoadShoppingCartFromCustomer(int.Parse(User.Identity.Name));

            var vmCart = new List<ShoppingCartIndexVM>();
            //ViewModel erstellen/mappen


            foreach (var item in dbCart)
            {
                var vmItem = new ShoppingCartIndexVM()
                {
                    ShoppingCartId = item.ShoppingCartId,
                    ProductId = item.ProductId,
                    Name = item.ProductName,
                    ImagePath = item.ImagePath,
                    UnitAmount = item.UnitAmount,
                    UnitAbbreviation = item.Unit,
                    Manufacturer = item.ManufacturerName,
                    Amount = item.Amount,
                    LinePrice = Helper.GetLinePrice(item.UnitPrice, item.Amount, item.TaxRate)
                };

                vmCart.Add(vmItem);
            }
            //View anzeigen mit daten
            decimal summe = vmCart.Sum(a => a.LinePrice);
            ViewBag.summe = summe.ToString("0.00");

            return View(vmCart);
        }

        [Authorize]
        [HttpGet]
        public ActionResult RemoveProduct(int id)
        {
            var customerId = int.Parse(User.Identity.Name);
            bool success = ShoppingCartManager.Remove(customerId, id);

            if (success)
            {
                TempData["ErrorMessage"] = "Produkt erfolgreich aus Warenkorb entfernt";
                return RedirectToAction("Index", "Product");
            }

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        [HttpGet]
        public ActionResult Add(int pid)
        {
            return Add(pid, 1);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Add(int pid, int amount)
        {
            var customerId = int.Parse(User.Identity.Name);
            bool success = ShoppingCartManager.Add(customerId, pid, amount);

            if (success)
            {
                TempData["ConfirmMessage"] = ShoppingCartManager.productadded(pid, amount);
                return RedirectToAction("Index", "Product");
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult FinishOrder()
        {
            return FinishOrder(new OrderFinish());
        }

        [HttpPost]
        public ActionResult FinishOrder(OrderFinish vmOrderFinish)
        {
            var dbshoppingCart = ShoppingCartManager.LoadShoppingCartFromCustomer(int.Parse(User.Identity.Name));
            var dbPaytypes = ShoppingCartManager.getPaymentType();
            var dbDelievertimes = ShoppingCartManager.getDelievery();

            OrderFinish orderfinsh = new OrderFinish();


            foreach (var item in dbshoppingCart)
            {
                var vmItem = new ShoppingCartIndexVM()
                {
                    ShoppingCartId = item.ShoppingCartId,
                    ProductId = item.ProductId,
                    Name = item.ProductName,
                    ImagePath = item.ImagePath,
                    UnitAmount = item.UnitAmount,
                    UnitAbbreviation = item.Unit,
                    Manufacturer = item.ManufacturerName,
                    Amount = item.Amount,
                    LinePrice = Helper.GetLinePrice(item.UnitPrice, item.Amount, item.TaxRate)
                };

                orderfinsh.ShoppingCart.Add(vmItem);
            }

            decimal totalprice = orderfinsh.ShoppingCart.Sum(a => a.LinePrice);

            orderfinsh.ProductsTotal = totalprice;            
            orderfinsh.TotalPrice = totalprice+ Helper.CalculateCharges(vmOrderFinish.OrderId, vmOrderFinish.Payment, vmOrderFinish.DelieveryDate, vmOrderFinish.Delievery);

            foreach (var item in dbPaytypes)
            {
                var vmItem = new PaymentTypeVM()
                {
                    PaymentTypeId = item.Id,
                    PaymentTypeName = item.Name
                };

                orderfinsh.PaymentTypeList.Add(vmItem);
            }

            foreach (var item in dbDelievertimes)
            {
                var vmItem = new DelieveryTimeVM()
                {
                    DelieverTimeId = item.Id,
                    DelieverTimeName = item.Name,
                    From = item.From,
                    To = item.To
                };
                orderfinsh.DelieveryTimeList.Add(vmItem);
            }

            var dbCustomer = ShoppingCartManager.getCustomerInfo(int.Parse(User.Identity.Name));

            orderfinsh.RechnungsStreet = dbCustomer.Street;
            orderfinsh.RechnungsCity = dbCustomer.City;
            orderfinsh.RechnungsZip = dbCustomer.Zip;

            orderfinsh.Lieferadresse = dbCustomer.Street;
            orderfinsh.LieferCity = dbCustomer.City;
            orderfinsh.LieferZip = dbCustomer.Zip;

            orderfinsh.Titel = dbCustomer.Title;
            orderfinsh.Vorname = dbCustomer.FirstName;
            orderfinsh.Nachname = dbCustomer.LastName;

            orderfinsh.Delievery = 6;

            orderfinsh.OrderId = ShoppingCartManager.GetShoppingCartId(int.Parse(User.Identity.Name));

            DateTime deliveryDefaultValue = DateTime.Now;

            if ((int)deliveryDefaultValue.AddDays(3).DayOfWeek == 6)
                ViewBag.date = deliveryDefaultValue.AddDays(5).ToString("yyyy-MM-dd");
            else if ((int)deliveryDefaultValue.AddDays(3).DayOfWeek == 0)
                ViewBag.date = deliveryDefaultValue.AddDays(4).ToString("yyyy-MM-dd");
            else
                ViewBag.date = deliveryDefaultValue.AddDays(3).ToString("yyyy-MM-dd");

            //Überprüfen ob wert aus dem request kommen und behalten

            if (!string.IsNullOrEmpty(vmOrderFinish.Lieferadresse))
            {
                orderfinsh.Lieferadresse = vmOrderFinish.Lieferadresse;
            }
            if (!string.IsNullOrEmpty(vmOrderFinish.LieferCity))
            {
                orderfinsh.LieferCity = vmOrderFinish.LieferCity;    
            }
            if (vmOrderFinish.LieferZip!=default(int))
            {
                orderfinsh.LieferZip = vmOrderFinish.LieferZip;
            }
            if(vmOrderFinish.Delievery!=default(int))
            {
                orderfinsh.Delievery = vmOrderFinish.Delievery;
            }
            if (vmOrderFinish.Payment!=default(int))
            {
                orderfinsh.Payment = vmOrderFinish.Payment;
            }
            if (vmOrderFinish.DelieveryDate != default(DateTime))
            {
                ViewBag.date = vmOrderFinish.DelieveryDate.ToString("yyyy-MM-dd");
            }

            return View(orderfinsh);
            
        }

        [HttpPost]
        [Authorize]
        public ActionResult FinalizeOrder(OrderFinish vmOrderFinish)
        {
            //vmOrderFinish.DelieveryDate Tag?
            string tag = vmOrderFinish.DelieveryDate.DayOfWeek.ToString();

            //GEsamtpreis berechnen (Aufschläge berücksichtigen)
            var success = OrderManager.PlaceOrder(
                int.Parse(User.Identity.Name),
                vmOrderFinish.Payment,
                vmOrderFinish.DelieveryDate,
                vmOrderFinish.Delievery,
                vmOrderFinish.Lieferadresse,
                vmOrderFinish.LieferCity,
                vmOrderFinish.LieferZip);
            //Aus Warenkorb eine Bestellung machen (Neue Adresse falls geändert berücksichtigen)
            //Dem Benutzer eine Danke Seite anzeigen
            if (!success)
            {
                return RedirectToAction("Index", "Home");
            }


            return RedirectToAction("Thanks", new { id = vmOrderFinish.OrderId });
        }

        [HttpGet]
        [Authorize]
        public ActionResult Thanks(int id)
        {
            //id = OrderId 
            using (var db = new FeinkosthandelEntities())
            {
                var x = db.GetOrderInfo.ToList();
                ThanksVM vmThanks = new ThanksVM();
                var dbOrderInfo = db.GetOrderInfo.Where(o => o.OrderId == id).First();

                vmThanks.City = dbOrderInfo.City;
                vmThanks.Date = dbOrderInfo.Date;
                vmThanks.From = dbOrderInfo.From;
                vmThanks.To = dbOrderInfo.To;
                vmThanks.Street = dbOrderInfo.Street;
                vmThanks.TotalPriceGross = dbOrderInfo.TotalGross;
                vmThanks.Zip = dbOrderInfo.Zip;
                vmThanks.PaymentName = dbOrderInfo.Name;
                return View(vmThanks);
            }
            //gesucht Lieferdatum, Lieferzeitpunkt, Gesamtpreis

        }

        public ActionResult SearchIndex(string searchstring, int searchCat)
        {
            Session["selectionsearch"] = searchstring;
            Session["selectioncat"] = searchCat;

            var filteredList = ProductManager.getAllProducts()
                .FindAll(x => (x.CategoryName.ToLower().Contains(searchstring.ToLower()) || 
                    x.Name.ToLower().Contains(searchstring.ToLower()) || 
                    x.ManufacturerName.ToLower().Contains(searchstring.ToLower()) || 
                    x.Description.ToLower().Contains(searchstring.ToLower()))
                    && (searchCat!=0? x.CategoryId == searchCat:x.CategoryId!=0)
                    );

            var vmProducts = new List<ProductIndexVM>();

            foreach (var dbProduct in filteredList)
            {
                var vmProduct = new ProductIndexVM()
                {
                    Id = dbProduct.ProductId,
                    ProductName = dbProduct.Name,
                    Category = dbProduct.CategoryName,
                    UnitAmount = dbProduct.UnitAmount,
                    Manufacturer = dbProduct.ManufacturerName,
                    UnitPriceGross = dbProduct.UnitPriceGross ?? -1m,
                    Unit = dbProduct.UnitName,
                    UnitAbbreviation = dbProduct.UnitAbbreviation,
                    ImagePath = dbProduct.ImagePath,
                    CategoryId = dbProduct.CategoryId

                };

                if (vmProduct.UnitPriceGross == -1m)
                {
                    throw new Exception("Preis konnte nicht berechnet werden");
                }

                vmProducts.Add(vmProduct);
            }

            return View(vmProducts);

        }
    }
}