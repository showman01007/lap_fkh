﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Feinkosthandel.Logic;
using Feinkosthandel.Models;

namespace Feinkosthandel.Controllers
{
    public class ProductController : Controller
    {
        [Authorize]
        public ActionResult Index(int? id)
        {

            var dbProducts = ProductManager.getAllProductsByCategory(id ?? 1);

            var vmProducts = new List<ProductIndexVM>();

            foreach (var dbProduct in dbProducts)
            {
                var vmProduct = new ProductIndexVM()
                {
                    Id = dbProduct.ProductId,
                    ProductName = dbProduct.Name,
                    Category = dbProduct.CategoryName,
                    UnitAmount = dbProduct.UnitAmount,
                    Manufacturer = dbProduct.ManufacturerName,
                    UnitPriceGross = dbProduct.UnitPriceGross ?? -1m,
                    Unit = dbProduct.UnitName,
                    UnitAbbreviation = dbProduct.UnitAbbreviation,
                    ImagePath = dbProduct.ImagePath,
                    CategoryId = dbProduct.CategoryId,
                    CampWas = dbProduct.CampWas ?? 0

                };

                if (vmProduct.UnitPriceGross == -1m)
                {
                    throw new Exception("Preis konnte nicht berechnet werden");
                }

                vmProducts.Add(vmProduct);
            }

            return View(vmProducts);
        }

        [Authorize]
        public ActionResult Details(int id)
        {
            var dbProduct = ProductManager.getProductById(id);

            if (dbProduct == null)
                return RedirectToAction("Index");

            var vmProduct = new ProductDetailVM()
            {
                Id = dbProduct.ProductId,
                Name = dbProduct.Name,
                Category = dbProduct.CategoryName,
                CategoryTaxRate = dbProduct.CategoryTaxRate,
                Manufacturer = dbProduct.ManufacturerName,
                UnitPriceGross = dbProduct.UnitPriceGross ?? -1,
                UnitPrice = dbProduct.UnitPrice,
                Unit = dbProduct.UnitName,
                UnitAmount = dbProduct.UnitAmount,
                UnitAbbreviation = dbProduct.UnitAbbreviation,
                ImagePath = dbProduct.ImagePath,
                Description = dbProduct.Description
            };
            if (vmProduct.UnitPriceGross == -1)
            {
                throw new Exception("Preis konnt nicht berechnet werden");
            }
            return View(vmProduct);
        }
    }
}