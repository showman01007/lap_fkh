﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Feinkosthandel.Models;
using Feinkosthandel.Logic;

namespace Feinkosthandel.Controllers
{
    public class NavigationController : Controller
    {
        // GET: Navigation
        public ActionResult Menu()
        {
            List<CategoryVM> catlist = ProductManager.getAllCategories();
            return PartialView("_Navigation",catlist);
        }

        public ActionResult Cats()
        {            
            var selCat = 0;
            Session["selectionsearch"] = string.IsNullOrEmpty((string)Session["selectionsearch"]) ? "" : Session["selectionsearch"];
            try
            {
                selCat = (int)Session["selectioncat"];
            }
            catch
            {
                selCat = 0;
            }
            finally
            {
                Session["selectioncat"] = selCat;
            }

            CategorySearchVM catSearcher = new CategorySearchVM();
            catSearcher.Catlist.Add(new CategoryVM() { id = 0, name = "Alle" });
            List<CategoryVM> liste = ProductManager.getAllCategories();
            foreach (var item in liste)
            {
                catSearcher.Catlist.Add(item);
            }
            
            catSearcher.CatSelected = 0;
            catSearcher.categoryName = "";
            if (catSearcher.CatSelected != default(int))
                catSearcher.CatSelected = (int)TempData["selectioncat"];
            if (!string.IsNullOrEmpty(catSearcher.categoryName))
                catSearcher.categoryName = TempData["selectionsearch"] as string;
            return PartialView("_Categories",catSearcher);
        }
    }
}