﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Feinkosthandel
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            //Cookie Abrufen und auf Existenz prüfen
            var authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie == null || authCookie.Value == "")
            {
                return;
            }

            try
            {
                //Cookie Inhalt (= Auth Ticket) entschlüsseln
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                //Rollenmanagement 
                var roleString = authTicket.UserData;
                var userRoles = new string[1];
                userRoles[0] = roleString;

                //Identität erzeugen
                var authIdentity = new GenericIdentity(authTicket.Name);
                var authPrincipal = new GenericPrincipal(authIdentity, userRoles);

                //Identität unserer Webanwendung mitteilen
                Context.User = authPrincipal;
            }
            catch
            {
                return;
            }
        }
    }
}
