﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Feinkosthandel.Models;
using Feinkosthandel.Logic.DAL;

namespace Feinkosthandel.Logic
{
    public static class AdminManager
    {
        public static bool addProducts(AdminProductVM vmProducts)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return true;
            }
        }

        internal static void updateProducts(AdminProductVM vmProduct)
        {
            throw new NotImplementedException();
        }

        internal static List<ManufacturerVM> getAllManufacturer()
        {
            using (var db = new FeinkosthandelEntities())
            {
                var dbManufacturer = db.Manufactorer.ToList();
                List<ManufacturerVM> manulist = new List<ManufacturerVM>();
                foreach (var item in dbManufacturer)
                {
                    manulist.Add(new ManufacturerVM()
                    {
                        ManuId = item.Id,
                        ManufacturerName = item.Name
                    });
                }
                return manulist;
            }
        }

        internal static List<UnitVM> getAllUnits()
        {
            using (var db = new FeinkosthandelEntities())
            {
                var dbUnits = db.Unit.ToList();
                List<UnitVM> unitlist = new List<UnitVM>();
                foreach (var item in dbUnits)
                {
                    unitlist.Add(new UnitVM()
                    {
                        UnitId = item.Id,
                        UnitAbbreviation = item.Abbreviation,
                        UnitName = item.Name
                    });
                }
                return unitlist;
            }
        }

        internal static bool createUnit(string unitname, string unitabbreviation)
        {
            using (var db = new FeinkosthandelEntities())
            {
                db.Entry(new Unit() { Name = unitname, Abbreviation = unitabbreviation }).State = System.Data.Entity.EntityState.Added;
                return db.SaveChanges() == 1;
            }
        }

        internal static bool createCategory(string categoryname)
        {
            using (var db = new FeinkosthandelEntities())
            {
                db.Entry(new Category() { Name = categoryname, TaxRate = 10 }).State = System.Data.Entity.EntityState.Added;
                return db.SaveChanges() == 1;
            }
        }

        internal static bool createManufacturer(string manufacturername)
        {
            using (var db = new FeinkosthandelEntities())
            {
                db.Entry(new Manufactorer() { Name = manufacturername }).State = System.Data.Entity.EntityState.Added;
                return db.SaveChanges() == 1;
            }
        }

        internal static bool deleteUnit(int id)
        {
            using (var db = new FeinkosthandelEntities())
            {
                db.Entry(db.Unit.Find(id)).State = System.Data.Entity.EntityState.Deleted;
                return db.SaveChanges() == 1;
            }
        }

        internal static bool deleteCategory(int id)
        {
            using (var db = new FeinkosthandelEntities())
            {
                db.Entry(db.Category.Find(id)).State = System.Data.Entity.EntityState.Deleted;
                return db.SaveChanges() == 1;
            }
        }

        internal static bool deleteManufacturer(int id)
        {
            using (var db = new FeinkosthandelEntities())
            {
                db.Entry(db.Manufactorer.Find(id)).State = System.Data.Entity.EntityState.Deleted;
                return db.SaveChanges() == 1;
            }
        }
    }
}