﻿using Feinkosthandel.Logic.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Feinkosthandel.Logic
{
    public static class Helper
    {
        public static decimal GetLinePrice(decimal unitPrice, int amount, decimal taxRate)
        {
            return (amount * unitPrice) * (1 + taxRate / 100);
        }

        public static bool IsDateWeekend(DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday ||date.DayOfWeek == DayOfWeek.Sunday;
        }

        public static bool IsNDaysFromToday(DateTime date, int numDaysAway)
        {
            return DateTime.Now.Date.AddDays(numDaysAway) == date.Date;
        }

        public static PaymentType GetPaymentCharge(int paymentId)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.PaymentType.Where(pt => pt.Id == paymentId).FirstOrDefault();
            }
        }

        public static decimal GetDeliveryCharge(DateTime deliveryDate, int deliveryTimeId)
        {
            try
            {
                using (var db = new FeinkosthandelEntities())
                {
                    var dbCharge = db.Charge.ToList();
                    var deliveryCharges = 0m;
                    if (IsDateWeekend(deliveryDate))
                    {
                        deliveryCharges += dbCharge.Where(c => c.Name == "Weekend Delievery").FirstOrDefault().Rate;
                    }

                    if (IsNDaysFromToday(deliveryDate,1))
                    {
                        deliveryCharges += dbCharge.Where(c => c.Name == "Next Day Delievery").FirstOrDefault().Rate;
                    }

                    if (IsNDaysFromToday(deliveryDate, 2))
                    {
                        deliveryCharges += dbCharge.Where(c => c.Name == "Two Day Delievery").FirstOrDefault().Rate;
                    }

                    if(deliveryTimeId != 6)
                    {
                        deliveryCharges += dbCharge.Where(c => c.Name == "Specific Delievery").FirstOrDefault().Rate;
                    }

                    return deliveryCharges;
                }
            }
            catch (Exception e)
            {
                return 0m;
            }
        }

        public static decimal CalculateCharges(int orderId, int paymentTypeId, DateTime deliveryDate, int deliveryTimeId)
        {
            using (var db = new FeinkosthandelEntities())
            {
                var dbProducts = db.ShoppingCart.Where(o => o.ShoppingCartId == orderId).ToList();

                decimal productsNet = 0m;

                foreach (var prod in dbProducts)
                {
                    productsNet += prod.Amount * prod.UnitPrice;
                }

                var deliveryChargeRate = GetDeliveryCharge(deliveryDate, deliveryTimeId);
                var deliveryChargeTax = 10m;

                var paymentChargeRate = GetPaymentCharge(paymentTypeId)?.ChargeRate ?? 0m;
                var paymentChargeTax = GetPaymentCharge(paymentTypeId)?.TaxRate ?? 0m;

                var deliveryChargeTotal = 0m;
                var paymentChargeTotal = 0m;

                if (paymentChargeRate != 0m)
                {
                    var paymentChargeNet = paymentChargeRate / 100 * productsNet;
                    paymentChargeTotal = paymentChargeNet + (paymentChargeNet * paymentChargeTax / 100);
                }

                if (deliveryChargeRate != 0)
                {
                    var deliveryChargeNet = deliveryChargeRate / 100 * productsNet;
                    deliveryChargeTotal = deliveryChargeNet + (deliveryChargeNet * deliveryChargeTax / 100);
                }

                return deliveryChargeTotal + paymentChargeTotal;
            }
        }
    }
}