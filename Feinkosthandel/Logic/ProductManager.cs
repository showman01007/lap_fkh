﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Feinkosthandel.Models;
using Feinkosthandel.Logic.DAL;

namespace Feinkosthandel.Logic
{
    public static class ProductManager
    {
        public static List<ProductInfo> getAllProducts()
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.ProductInfo.ToList();
            }
        }

        public static List<ProductInfo> getAllProductsByCategory(int id)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.ProductInfo.Where(p => p.CategoryId == id).ToList();
            }
        }

        public static List<CategoryVM> getAllCategories()
        {
            using (var db = new FeinkosthandelEntities())
            {
                var result = db.Category.ToList();
                List<CategoryVM> vmCategoryList = new List<CategoryVM>();
                foreach(var category in result)
                {
                    vmCategoryList.Add(new CategoryVM
                    {
                        id = category.Id,
                        name = category.Name,
                        productcounter = getAllProductsByCategory(category.Id).Count()
                    });
                }
                return vmCategoryList;
            }
        }

        public static ProductInfo getProductById(int id)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.ProductInfo.Where(model => model.ProductId==id).FirstOrDefault();
            }
        }
    }
}