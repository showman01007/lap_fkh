﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Feinkosthandel.Logic.DAL;

namespace Feinkosthandel.Logic
{
    public static class OrderManager
    {
        public static bool PlaceOrder(int customerId, int paymentTypeId, DateTime delieveryTime, int deliveryTimeId, string street, string city, int zip)
        {
            //OrderId des Warenkorbs laden
            var shoppingCartId = ShoppingCartManager.SearchShoppingCartId(customerId);
            var dbOrder = LoadOrder(shoppingCartId);
            if (dbOrder == null)
                return false;

            dbOrder.Date = DateTime.Now;
            //Payment für die Order anlegen
            var dbPayment = new Payment()
            {
                Date = null,
                OrderId = shoppingCartId,
                TotalGross = CalculateTotalGross(shoppingCartId,paymentTypeId,delieveryTime, deliveryTimeId),
                Type = paymentTypeId
            };

            //Delievery für die Order anlegen, 
            var dbDelivery = new Delivery()
            {
                OrderId = shoppingCartId,
                Date = delieveryTime,
                DeliveryTimeId = deliveryTimeId,
                ChargeRate =Helper.GetDeliveryCharge(delieveryTime, deliveryTimeId),
                //Auschläge
                TaxRate = 10m,
                Street = street,
                City = city,
                Zip = zip
            };

            using (var db = new FeinkosthandelEntities())
            {
                db.Entry(dbPayment).State = System.Data.Entity.EntityState.Added;
                db.Entry(dbDelivery).State = System.Data.Entity.EntityState.Added;
                db.Entry(dbOrder).State = System.Data.Entity.EntityState.Modified;

                return db.SaveChanges() == 3;
            }                      
        }

        private static decimal CalculateTotalGross(int shoppingCartId, int paymentTypeId, DateTime deliveryDate, int deliveryTimeId)
        {

            return CalculateProductGrossTotal (shoppingCartId) + Helper.CalculateCharges(shoppingCartId,paymentTypeId, deliveryDate, deliveryTimeId);
        }

        private static decimal CalculateProductGrossTotal(int orderId)
        {
            //Product Gross Total berechnen
            using (var db = new FeinkosthandelEntities())
            {
                var dbOrder = db.ShoppingCart.Where(sc => sc.ShoppingCartId == orderId).ToList();
                var total = 0m;
                foreach (var product in dbOrder)
                {
                    total += Helper.GetLinePrice(product.UnitPrice, product.Amount, product.TaxRate);
                }
                return total;
            }
        }

        private static decimal CalculateChargesGrossTotal()
        {
            //Aufschläge berechnen
            return 0m;
        }

        private static Order LoadOrder(int orderId)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.Order.Where(order => order.Id == orderId).FirstOrDefault();
            }

        }
    }
}