//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Feinkosthandel.Logic.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductInfo
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal UnitAmount { get; set; }
        public string ImagePath { get; set; }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public string UnitAbbreviation { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public decimal CategoryTaxRate { get; set; }
        public Nullable<decimal> UnitPriceGross { get; set; }
        public Nullable<decimal> PricePerOneUnit { get; set; }
        public Nullable<int> CampWas { get; set; }
    }
}
