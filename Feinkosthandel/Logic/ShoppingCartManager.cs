﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Feinkosthandel.Logic.DAL;
using Feinkosthandel.Models;

namespace Feinkosthandel.Logic
{
    public static class ShoppingCartManager
    {
        public static bool Add(int customerId, int productId, int amount)
        {
            if (productId <= 0 || amount <= 0 || customerId <= 0)
            {
                return false;
            }

            var dbShoppingCartId = GetShoppingCartId(customerId);

            var success = AddProductToShoppingCart(dbShoppingCartId, productId, amount);

            return success;
        }

        public static bool Remove(int customerId, int productId)
        {
            if (productId <= 0 || customerId <= 0)
            {
                return false;
            }

            var dbShoppingCartId = GetShoppingCartId(customerId);

            var success = RemoveProductFromShoppingCart(dbShoppingCartId, productId);

            return success;
        }

        private static bool RemoveProductFromShoppingCart(int shoppingCartId, int productId)
        {
            using (var db = new FeinkosthandelEntities())
            {
                var dbOrderLine = db.OrderLine.Where(s => s.OrderId == shoppingCartId && s.ProductId == productId).FirstOrDefault();
                if (dbOrderLine == null)
                    return false;

                var dbWarehouse = db.Warehouse.Where(w => w.productId == productId).FirstOrDefault();
                dbWarehouse.campWas += dbOrderLine.NumUnits;
                db.Entry(dbWarehouse).State = EntityState.Modified;

                db.Entry(dbOrderLine).State = EntityState.Deleted;
                return db.SaveChanges()==2;
            }
        }

        public static List<PaymentType> getPaymentType()
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.PaymentType.ToList();
            }
        }

        public static List<DeliveryTime> getDelievery()
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.DeliveryTime.ToList();
            }
        }

        public static Customer getCustomerInfo(int customerid)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.Customer.Find(customerid);
            }
        }

        public static OrderFinshViewNeu loadOrderFinishFromDataBase(int orderId)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.OrderFinshViewNeu.Where(o => o.ShoppingCartId == orderId).FirstOrDefault();
            }
        }

        public static List<ShoppingCart> LoadShoppingCartFromCustomer(int customerId)
        {
            var shoppingCartId = GetShoppingCartId(customerId);

            using (var db = new FeinkosthandelEntities())
            {
                return db.ShoppingCart.Where(sc => sc.ShoppingCartId == shoppingCartId).ToList();
            }
        }

        public static int GetShoppingCartId(int customerId)
        {
            int shoppingCartId = -1;
            //1. nach warenkorb suchen
            shoppingCartId = SearchShoppingCartId(customerId);

            //2. wenn nicht, dann neuen anlegen
            if (shoppingCartId == -1)
            {
                shoppingCartId = CreateNewShoppingCart(customerId);
            }
            return shoppingCartId;
        }

        internal static int SearchShoppingCartId(int customerId)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.SearchShoppingCartId(customerId).FirstOrDefault() ?? -1;
            }
        }

        private static int CreateNewShoppingCart(int customerId)
        {
            var dbOrder = new Order()
            {
                CustomerId = customerId,
                Date = DateTime.Now
            };

            using (var db = new FeinkosthandelEntities())
            {
                db.Order.Add(dbOrder);
                if (db.SaveChanges() != 1)
                {
                    return -1;
                }
            }

            return dbOrder.Id;
        }

        private static bool AddProductToShoppingCart(int shoppingCartId, int productId, int amount)
        {
            bool success = false;
            if (shoppingCartId <= 0)
            {
                return false;
            }

            if (IsProductInShoppingCart(shoppingCartId, productId))
            {
                success = AddAmountToShoppingCart(shoppingCartId, productId, amount);
            }
            else
            {
                success = AddNewOrderLineToShoppingCart(shoppingCartId, productId, amount);
            }

            return success;
        }

        private static bool AddNewOrderLineToShoppingCart(int shoppingCartId, int productId, int amount)
        {
            using (var db = new FeinkosthandelEntities())
            {
                var dbProduct = db.Product.Where(p => p.Id == productId)
                    .Include(p=>p.Category)
                    .FirstOrDefault();
                
                var dbOrderLine = new OrderLine()
                {
                    OrderId = shoppingCartId,
                    ProductId = productId,
                    TaxRate = dbProduct.Category.TaxRate,
                    UnitPrice = dbProduct.UnitPrice,
                    NumUnits = amount
                };
                var dbWarehouseAmount = db.Warehouse.Where(w => w.productId == productId).FirstOrDefault();
                dbWarehouseAmount.campWas -= amount;
                db.Entry(dbWarehouseAmount).State = EntityState.Modified;
                db.OrderLine.Add(dbOrderLine);
                return db.SaveChanges() == 1;
            }
            
        }

        private static bool AddAmountToShoppingCart(int shoppingCartId, int productId, int amount)
        {
            using (var db = new FeinkosthandelEntities())
            {
                var dbOrderLine = db.OrderLine.Where(ol => ol.OrderId == shoppingCartId && ol.ProductId == productId).FirstOrDefault();
                dbOrderLine.NumUnits += amount;

                var dbproductWareAmount = db.Warehouse.Where(w => w.productId == productId).FirstOrDefault();
                dbproductWareAmount.campWas -= amount;

                db.Entry(dbOrderLine).State = EntityState.Modified;
                db.Entry(dbproductWareAmount).State = EntityState.Modified;
                
                return db.SaveChanges()==2;
            }
        }

        private static bool IsProductInShoppingCart(int shoppingCart, int productId)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.OrderLine.Where(ol => ol.OrderId == shoppingCart && ol.ProductId == productId).Any();
            }
        }

        public static string productadded(int pid, int amount)
        {
            using (var db = new FeinkosthandelEntities())
            {
                var dbProduct = db.OrderLine.Where(ol => ol.ProductId == pid).FirstOrDefault();
                return $"{dbProduct.Product.Name} wurde {amount} x hinzugefügt";
            }
        }
    }
}