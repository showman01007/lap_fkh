﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

using Feinkosthandel.Logic.DAL;

namespace Feinkosthandel.Logic
{
    public static class CustomerManager
    {
        public static bool addNewCustomer(string city, string email, string firstname, string lastname, string street, string title, int zipCode, string password)
        {

            if( DoesEmailExist(email))
            {
                return false;
            }

            var salt = GenerateSalt();

            var saltedPassword = password + BytesToString(salt);

            var hashAndSaltedPassword = HashString(saltedPassword);

            var dbAccount = new Account()
            {
                Email = email,
                PwHash = hashAndSaltedPassword,
                Salt = salt
            };

            var dbCustomer = new Customer()
            {
                Title = title,
                FirstName = firstname,
                LastName = lastname,
                Zip = zipCode,
                City = city,
                Street = street
            };
            try
            {
                using (var db = new FeinkosthandelEntities())
                {
                    db.Customer.Add(dbCustomer);
                    var success = db.SaveChanges() == 1;
                    if (!success)
                    {
                        return false;
                    }
                    dbAccount.CustomerId = dbCustomer.id;

                    db.Account.Add(dbAccount);

                    success = db.SaveChanges() == 1;

                    if (!success)
                    {
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
                
        }

        public static bool IsLoginValid(string email, string password)
        {
            Account dbAccount = LoadAccount(email);

            if(dbAccount == null)
            {
                return false;
            }

            var saltedPassword = password + BytesToString(dbAccount.Salt);

            var hashedAndSaltedPw = HashString(saltedPassword);

            if( hashedAndSaltedPw.SequenceEqual(dbAccount.PwHash))
            {
                return true;
            }

            return false;
        }

        public static Account LoadAccount(string email)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.Account.Where(a => a.Email == email).FirstOrDefault();
            }
        }

        private static byte[] HashString(string str)
        {
            var stringBytes = System.Text.Encoding.UTF8.GetBytes(str);

            using (var hasher = new SHA256Managed())
            {
                var hash = hasher.ComputeHash(stringBytes);
                return hash;
            }
                
        }

        private static string BytesToString(byte[] bytes)
        {
            string hexString = "";
            foreach(var b in bytes)
            {
                hexString += b.ToString("X2");
            }
            return hexString;
        }

        private static byte[] GenerateSalt()
        {
            var salt = new byte[32];
            var rng = new RNGCryptoServiceProvider();
            rng.GetNonZeroBytes(salt);

            return salt;
        }

        private static bool DoesEmailExist(string email)
        {
            using (var db = new FeinkosthandelEntities())
            {
                return db.Account.Any(a => a.Email == email);
            }
                
        }
    }
}